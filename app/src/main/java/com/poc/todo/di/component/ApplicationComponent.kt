package com.poc.todo.di.component

import android.app.Application
import com.poc.todo.di.module.ApplicationModule
import com.poc.todo.di.module.DbModule
import com.poc.todo.di.module.RetrofitModule
import com.poc.todo.di.scope.ApplicationScope
import dagger.BindsInstance
import dagger.Component


@Component(modules = [ApplicationModule::class, DbModule::class, RetrofitModule::class])
@ApplicationScope
interface ApplicationComponent{
    fun plusFragmentComponent() : FragmentComponent


    @Component.Builder
    interface Builder {
        fun build(): ApplicationComponent
        @BindsInstance
        fun application(application: Application): Builder
    }

}