package com.poc.todo.di.qualifier

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContext