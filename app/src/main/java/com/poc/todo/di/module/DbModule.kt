package com.poc.todo.di.module

import android.app.Application
import androidx.room.Room
import com.poc.todo.data.database.MovieDb
import com.poc.todo.data.database.TaskDb
import com.poc.todo.di.scope.ApplicationScope
import dagger.Module
import dagger.Provides

@Module
class DbModule() {

    @Provides
    @ApplicationScope
    fun provideMovieDb(application: Application) =
        Room.databaseBuilder(
            application,
            MovieDb::class.java, "movie.db"
        ).build()

    @Provides
    @ApplicationScope
    fun provideMovieDao(movieDb: MovieDb) = movieDb.movieDao()

    @Provides
    @ApplicationScope
    fun provideTaskDb(application: Application) =
        Room.databaseBuilder(
            application,
            TaskDb::class.java, "task.db"
        ).build()

    @Provides
    @ApplicationScope
    fun provideTaskDao(taskDb: TaskDb) = taskDb.taskDao()
}