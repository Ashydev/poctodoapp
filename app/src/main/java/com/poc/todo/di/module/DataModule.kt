package com.poc.todo.di.module

import com.poc.todo.data.database.LocalDataImpl
import com.poc.todo.data.network.RemoteDataImpl
import com.poc.todo.data.repository.LocalData
import com.poc.todo.data.repository.RemoteData
import com.poc.todo.di.scope.FragmentScope
import dagger.Binds
import dagger.Module

@Module
interface DataModule{

    @Binds
    @FragmentScope
    fun bindLocalData (localData : LocalDataImpl) : LocalData

    @Binds
    @FragmentScope
    fun bindRemoteData (remoteData : RemoteDataImpl) : RemoteData

}