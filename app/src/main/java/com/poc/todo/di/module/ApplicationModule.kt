package com.poc.todo.di.module

import com.poc.todo.di.scope.ApplicationScope
import com.poc.todo.domain.Logger
import com.poc.todo.utils.TimberLogger
import dagger.Binds
import dagger.Module

@Module(includes = [ApplicationModule.LoggerModule::class])
class ApplicationModule() {
    @Module
    interface LoggerModule{
        @Binds
        @ApplicationScope
        fun bindLogger(loagger : TimberLogger) : Logger
    }
}