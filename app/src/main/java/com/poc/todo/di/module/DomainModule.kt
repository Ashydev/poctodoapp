package com.poc.todo.di.module

import com.poc.todo.data.repository.MovieDetailRepositoryImpl
import com.poc.todo.data.repository.MovieListRepositoryImpl
import com.poc.todo.data.repository.TaskListRepositoryImpl
import com.poc.todo.di.scope.FragmentScope
import com.poc.todo.domain.moviedetail.MovieDetailRepository
import com.poc.todo.domain.movielist.MovieListRepository
import com.poc.todo.domain.tasklist.TaskListRepository
import dagger.Binds
import dagger.Module

@Module
interface DomainModule {

    @Binds
    @FragmentScope
    fun bindMovieListRepository(movieListRepository: MovieListRepositoryImpl): MovieListRepository

    @Binds
    @FragmentScope
    fun bindMovieDetailRepository(movieDetailRepository: MovieDetailRepositoryImpl): MovieDetailRepository

    @Binds
    @FragmentScope
    fun bindTaskListRepository(taskListRepository: TaskListRepositoryImpl): TaskListRepository
}