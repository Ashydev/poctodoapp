package com.poc.todo.di.component

import com.poc.todo.di.module.DataModule
import com.poc.todo.di.module.DomainModule
import com.poc.todo.di.scope.FragmentScope
import com.poc.todo.ui.home.HomeFragment
import com.poc.todo.ui.home.HomeFragmentNew
import dagger.Subcomponent

@Subcomponent(modules = [DataModule::class, DomainModule::class])
@FragmentScope
interface FragmentComponent {
    fun inject(homeFragment: HomeFragment)
    fun inject(homeFragmentNew: HomeFragmentNew)
    //fun inject(homeDetailFragment: HomeDetailFragment)
}