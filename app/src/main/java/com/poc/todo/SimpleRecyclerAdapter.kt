package com.poc.todo

import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.poc.todo.utils.dp2px

class SimpleRecyclerAdapter(private val items: List<String>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(parent)

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ViewHolder).bind(items[position])
    }

    override fun getItemCount() = items.size

    class ViewHolder(parent: ViewGroup) : RecyclerView.ViewHolder(
            TextView(parent.context).apply {
                val p = dp2px(16).toInt()
                setPadding(p, p, p, p)
            }) {
        fun bind(item: String) = with(itemView as TextView) {
            text = item
        }
    }
}