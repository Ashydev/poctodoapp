package com.poc.todo

import android.app.Application
import com.poc.todo.di.component.ApplicationComponent
import com.poc.todo.di.component.DaggerApplicationComponent
import com.poc.todo.utils.PrefixDebugTree
import timber.log.Timber

class CKTodoApp : Application() {

    private val appComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent
            .builder()
            .application(this)
            .build()
    }



    override fun onCreate() {
        super.onCreate()
        instance = this
        if (BuildConfig.DEBUG) {
            Timber.plant(PrefixDebugTree())
        }
    }

    companion object {
        lateinit var instance: CKTodoApp
            private set
    }

    fun getApplicationComponent(): ApplicationComponent = appComponent
}
