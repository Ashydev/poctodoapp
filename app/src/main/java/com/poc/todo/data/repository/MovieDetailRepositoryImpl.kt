package com.poc.todo.data.repository

import androidx.lifecycle.LiveData
import com.poc.todo.domain.entity.Movie
import com.poc.todo.domain.moviedetail.MovieDetailRepository
import javax.inject.Inject


class MovieDetailRepositoryImpl @Inject constructor(
    private val localData: LocalData
) : MovieDetailRepository {
    override
    fun getMovie(id : Long): LiveData<Movie> {
        return localData.getMovie(id)
    }
}
