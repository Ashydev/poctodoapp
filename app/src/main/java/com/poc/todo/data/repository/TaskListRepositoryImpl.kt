package com.poc.todo.data.repository

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.poc.todo.data.TaskData
import com.poc.todo.domain.tasklist.TaskListRepository
import com.poc.todo.domain.vo.BoundaryState
import com.poc.todo.domain.vo.LoadingStatus
import com.poc.todo.domain.vo.Status
import com.poc.todo.utils.AppExecutors
import timber.log.Timber
import java.util.*
import javax.inject.Inject


class TaskListRepositoryImpl @Inject constructor(
    private val appExecutors: AppExecutors,
    private val localData: LocalData,
    private val remoteData: RemoteData
) : TaskListRepository {


    private val loadingStatus = MutableLiveData<LoadingStatus>()
    override fun insertTasks(tasks: List<TaskData>) {
        localData.insertTasks(tasks)

    }

    override
    fun getTasks(): LiveData<PagedList<TaskData>> {
        return localData.getTasks()
    }

    override fun getBoundaryState(): LiveData<BoundaryState<Date>> {
        return localData.getBoundaryState()
    }

    override
    fun fetchMore(fetchDate: Date, predicate: (String?) -> (Boolean)): LiveData<LoadingStatus> {
        if (loadingStatus.value == null || loadingStatus.value?.status != Status.LOADING) {
            loadingStatus.value = LoadingStatus.loading()
            Timber.d("fetchMore starting: %s", fetchDate)

            remoteData.fetchItems(fetchDate, { movies ->
                appExecutors.diskIO().execute {
                    localData.insertMovies(movies.filter { predicate(it.posterPath) })
                    Timber.d("fetchMore saved: %s", fetchDate)
                }
            }, { status ->
                loadingStatus.value = status
            })
        } else {
            Timber.d("fetchMore already loading %s", fetchDate)
        }
        return loadingStatus
    }

    override
    fun returnLoadingOrSuccess(): LiveData<LoadingStatus> {
        if (loadingStatus.value == null || loadingStatus.value?.status != Status.LOADING) {
            loadingStatus.value = LoadingStatus.success()
        }
        return loadingStatus
    }
}
