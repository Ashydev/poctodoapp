package com.poc.todo.data.database

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.paging.PagedList
import com.poc.todo.data.TaskData
import com.poc.todo.domain.vo.BoundaryState
import timber.log.Timber
import java.util.*

class TaskBoundaryCallback : PagedList.BoundaryCallback<TaskData>() {

    private val _boundaryState = MutableLiveData<BoundaryState<Date>>()
    val boundaryState: LiveData<BoundaryState<Date>>
        get() = _boundaryState

    companion object {
        const val DATABASE_PAGE_SIZE = 15
    }

    override fun onItemAtFrontLoaded(itemAtFront: TaskData) {
        Timber.d(
            "onItemAtFrontLoaded %d %s %s ", itemAtFront.id,
            itemAtFront.title, itemAtFront.hashCode()
        )
        _boundaryState.value = BoundaryState.itemLoadedAtTop(itemAtFront.date)
    }

    override fun onZeroItemsLoaded() {
        Timber.d("onZeroItemsLoaded")
        _boundaryState.value = BoundaryState.zeroItemsLoaded(Date())
    }

    override fun onItemAtEndLoaded(itemAtEnd: TaskData) {
        Timber.d(
            "onItemAtFrontLoaded %d %s %s ", itemAtEnd.id,
            itemAtEnd.title, itemAtEnd.hashCode()
        )
        _boundaryState.value = BoundaryState.itemLoadedAtBottom(itemAtEnd.date)
    }

    fun refresh() {
        Timber.d("refresh")
        _boundaryState.value = BoundaryState.zeroItemsLoaded(Date())
    }

}