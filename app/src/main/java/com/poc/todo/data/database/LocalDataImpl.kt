package com.poc.todo.data.database

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.poc.todo.data.MovieData
import com.poc.todo.data.TaskData
import com.poc.todo.data.repository.LocalData
import com.poc.todo.domain.entity.Movie
import com.poc.todo.domain.entity.MoviePoster
import com.poc.todo.domain.vo.BoundaryState
import com.poc.todo.utils.AppExecutors
import java.util.*
import javax.inject.Inject

class LocalDataImpl @Inject constructor(
    private val appExecutors: AppExecutors,
    private val movieDao: MovieDao,
    private val taskDao: TaskDao
) :
    LocalData {
    private val boundaryCallback =
        MovieBoundaryCallback()
    private val taskBoundaryCallback = TaskBoundaryCallback()
    override fun getTasks(): LiveData<PagedList<TaskData>> {
        val dataSourceFactory = taskDao.getDataSourcefactory()
        return LivePagedListBuilder(dataSourceFactory, TaskBoundaryCallback.DATABASE_PAGE_SIZE)
            .setBoundaryCallback(taskBoundaryCallback)
            .build()
    }

    override fun insertTasks(tasks: List<TaskData>) {
        appExecutors.diskIO().execute {
            taskDao.insertTasks(tasks)
        }
    }

    override fun deleteTask() {
        taskDao.deleteTasks()
    }


    override fun getMovies(): LiveData<PagedList<MoviePoster>> {
        val dataSourceFactory = movieDao.getDataSourcefactory()
        return LivePagedListBuilder(dataSourceFactory, MovieBoundaryCallback.DATABASE_PAGE_SIZE)
            .setBoundaryCallback(boundaryCallback)
            .build()
    }

    override fun getMovie(id: Long): LiveData<Movie> {
        return movieDao.getMovie(id)
    }

    override fun getBoundaryState(): LiveData<BoundaryState<Date>> {
        return boundaryCallback.boundaryState
    }

    override fun insertMovies(movies: List<MovieData>) {
        appExecutors.diskIO().execute {
            movieDao.insertMovies(movies)
        }
    }

    override fun deleteMovies() {
        movieDao.deleteMovies()
    }

    override fun refresh() {
        boundaryCallback.refresh()
    }
}