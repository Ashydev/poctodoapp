package com.poc.todo.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.poc.todo.data.TaskData

@Database(
    entities = [
        TaskData::class
    ],
    version = 1,
    exportSchema = false
)
@TypeConverters(DbTypeConverters::class)
abstract class TaskDb : RoomDatabase() {
    abstract fun taskDao(): TaskDao
}