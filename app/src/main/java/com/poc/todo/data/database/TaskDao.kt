package com.poc.todo.data.database

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.poc.todo.data.TaskData

@Dao
interface TaskDao {
    @Query("SELECT id, title, task, category, date,finish  FROM task ORDER BY date DESC, title ASC")
    fun getDataSourcefactory(): DataSource.Factory<Int, TaskData>

    @Query("SELECT * FROM task WHERE id= :id")
    fun getTask(id: Long): LiveData<TaskData>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertTasks(tasks: List<TaskData>)

    @Query("DELETE FROM task")
    fun deleteTasks()
}