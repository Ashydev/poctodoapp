package com.poc.todo.data

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity(tableName = "task")
data class TaskData (
    @PrimaryKey
    val id: Long = 0,
    val title: String,
    val task: String,
    val category: String,
    val date: Date,
    val finish: Boolean
)