package com.poc.todo.data.repository

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.poc.todo.data.MovieData
import com.poc.todo.data.TaskData
import com.poc.todo.domain.entity.Movie
import com.poc.todo.domain.entity.MoviePoster
import com.poc.todo.domain.vo.BoundaryState
import java.util.*

// This interface is in accordance to Dependency Inversion Principle by separating the higher
// repository class from lower database class.
interface LocalData {

    fun getMovies(): LiveData<PagedList<MoviePoster>>
    fun getBoundaryState(): LiveData<BoundaryState<Date>>
    fun insertMovies(movies: List<MovieData>)
    fun deleteMovies()
    fun refresh()

    fun getMovie(id: Long): LiveData<Movie>

    fun getTasks(): LiveData<PagedList<TaskData>>
    fun insertTasks(tasks: List<TaskData>)
    fun deleteTask()
}