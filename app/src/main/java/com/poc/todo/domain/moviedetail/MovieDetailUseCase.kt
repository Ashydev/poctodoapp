package com.poc.todo.domain.moviedetail

import androidx.lifecycle.LiveData
import com.poc.todo.domain.Logger
import com.poc.todo.domain.entity.Movie
import javax.inject.Inject

class MovieDetailUseCase @Inject constructor(private val repository: MovieDetailRepository, private val log : Logger){
    fun getMovie(id : Long): LiveData<Movie> {
        return repository.getMovie(id)
    }
}