package com.poc.todo.domain.tasklist

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.poc.todo.data.TaskData
import com.poc.todo.domain.vo.BoundaryState
import com.poc.todo.domain.vo.LoadingStatus
import java.util.*

interface TaskListRepository {
    fun insertTasks(tasks: List<TaskData>)
    fun getTasks(): LiveData<PagedList<TaskData>>
    fun getBoundaryState(): LiveData<BoundaryState<Date>>
    fun fetchMore(fetchDate: Date, predicate: (String?) -> (Boolean)): LiveData<LoadingStatus>
    fun returnLoadingOrSuccess(): LiveData<LoadingStatus>
}