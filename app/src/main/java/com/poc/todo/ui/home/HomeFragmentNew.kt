package com.poc.todo.ui.home


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.poc.todo.CKTodoApp
import com.poc.todo.R
import com.poc.todo.domain.vo.ErrorCode
import com.poc.todo.domain.vo.Status
import com.poc.todo.utils.OnReselectedDelegate
import com.poc.todo.utils.isSectionVisible
import com.poc.todo.utils.setupActionBar
import com.poc.todo.utils.toast
import com.example.cktravelapp.ui.fragments.home.HomeViewModelFactoryNew
import com.example.cktravelapp.ui.fragments.home.HomeViewModelNew
import kotlinx.android.synthetic.main.fragment_home_new.*
import javax.inject.Inject

class HomeFragmentNew : Fragment(), OnReselectedDelegate {

    private lateinit var taskListAdapter: HomeAdapterNew
    @Inject
    lateinit var taskListViewModelFactory: HomeViewModelFactoryNew

    companion object {
        private const val COL = 2
    }

    override fun onAttach(context: Context?) {
        CKTodoApp.instance.getApplicationComponent().plusFragmentComponent().inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val taskListViewModel = ViewModelProviders.of(
            this, taskListViewModelFactory
        )
            .get(HomeViewModelNew::class.java)
        /*recyclerViewNew.layoutManager = LinearLayoutManager(activity)
        recyclerViewNew.adapter = SimpleRecyclerAdapter((1..30).map { "Item $it" })
        recyclerViewNew.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
*/
        taskListAdapter = HomeAdapterNew()

        recyclerViewNew.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(activity)
            adapter = taskListAdapter
        }
        recyclerViewNew.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.VERTICAL))
        taskListViewModel.insertTasks()
        taskListViewModel.tasks.observe(viewLifecycleOwner, Observer { list ->
            taskListAdapter.submitList(list)
        })

        taskListViewModel.loadingStatus.observe(viewLifecycleOwner, Observer { loadingStatus ->
            when {
                loadingStatus?.status == Status.LOADING -> loadingProgressnew.visibility = View.VISIBLE
                loadingStatus?.status == Status.SUCCESS -> {
                    loadingProgressnew.visibility = View.INVISIBLE
                    //toggleRefreshing(false)
                }
                loadingStatus?.status == Status.ERROR -> {
                    loadingProgressnew.visibility = View.INVISIBLE
                    //toggleRefreshing(false)
                    showErrorMessage(loadingStatus.errorCode, loadingStatus.message)
                }
            }
        })

        /*swipeRefreshLayout.setOnRefreshListener {
            movieListViewModel.refresh()
        }*/
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home_new, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("HomeFragment", "onViewCreated")
        if (isSectionVisible()) setupActionBar()
    }

    /*private fun toggleRefreshing(refreshing: Boolean) {
        swipeRefreshLayout.isRefreshing = refreshing
    }*/

    private fun showErrorMessage(errorCode: ErrorCode?, message: String?) {
        when (errorCode) {
            ErrorCode.NO_DATA -> activity!!.toast(getString(R.string.error_no_data))
            ErrorCode.NETWORK_ERROR -> activity!!.toast(getString(R.string.error_network, message))
            ErrorCode.UNKNOWN_ERROR -> activity!!.toast(getString(R.string.error_unknown, message))
        }
    }


    private fun setupActionBar() = setupActionBar("Home")

    override fun onReselected() = setupActionBar()
}
