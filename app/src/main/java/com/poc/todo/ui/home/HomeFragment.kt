package com.poc.todo.ui.home


import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.poc.todo.CKTodoApp
import com.poc.todo.R
import com.poc.todo.domain.vo.ErrorCode
import com.poc.todo.domain.vo.Status
import com.poc.todo.utils.OnReselectedDelegate
import com.poc.todo.utils.isSectionVisible
import com.poc.todo.utils.setupActionBar
import com.poc.todo.utils.toast
import com.example.cktravelapp.ui.fragments.home.HomeViewModel
import com.example.cktravelapp.ui.fragments.home.HomeViewModelFactory
import kotlinx.android.synthetic.main.fragment_home.*
import javax.inject.Inject

class HomeFragment : Fragment(), OnReselectedDelegate {

    private lateinit var movieListAdapter: HomeAdapter
    @Inject
    lateinit var movieListViewModelFactory: HomeViewModelFactory

    companion object {
        private const val COL = 2
    }

    override fun onAttach(context: Context?) {
        CKTodoApp.instance.getApplicationComponent().plusFragmentComponent().inject(this)
        super.onAttach(context)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val movieListViewModel = ViewModelProviders.of(
            this, movieListViewModelFactory
        )
            .get(HomeViewModel::class.java)


       /* movieListAdapter = HomeAdapter {
            NavHostFragment.findNavController(this)
                .navigate(HomeFragmentDirections.actionNext(it.toString()))
        }*/

        movieList.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(activity, COL)
            adapter = movieListAdapter
        }
        movieListViewModel.movies.observe(viewLifecycleOwner, Observer { list ->
            movieListAdapter.submitList(list)
        })

        movieListViewModel.loadingStatus.observe(viewLifecycleOwner, Observer { loadingStatus ->
            when {
                loadingStatus?.status == Status.LOADING -> loadingProgress.visibility = View.VISIBLE
                loadingStatus?.status == Status.SUCCESS -> {
                    loadingProgress.visibility = View.INVISIBLE
                    toggleRefreshing(false)
                }
                loadingStatus?.status == Status.ERROR -> {
                    loadingProgress.visibility = View.INVISIBLE
                    toggleRefreshing(false)
                    showErrorMessage(loadingStatus.errorCode, loadingStatus.message)
                }
            }
        })

        swipeRefreshLayout.setOnRefreshListener {
            movieListViewModel.refresh()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_home, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        Log.d("HomeFragment", "onViewCreated")
        if (isSectionVisible()) setupActionBar()
    }

    private fun toggleRefreshing(refreshing: Boolean) {
        swipeRefreshLayout.isRefreshing = refreshing
    }

    private fun showErrorMessage(errorCode: ErrorCode?, message: String?) {
        when (errorCode) {
            ErrorCode.NO_DATA -> activity!!.toast(getString(R.string.error_no_data))
            ErrorCode.NETWORK_ERROR -> activity!!.toast(getString(R.string.error_network, message))
            ErrorCode.UNKNOWN_ERROR -> activity!!.toast(getString(R.string.error_unknown, message))
        }
    }


    private fun setupActionBar() = setupActionBar("Home")

    override fun onReselected() = setupActionBar()
}
