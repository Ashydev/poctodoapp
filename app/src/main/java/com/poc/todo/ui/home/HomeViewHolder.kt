package com.example.cktravelapp.ui.fragments.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.poc.todo.R
import com.poc.todo.domain.ResourceUrl
import com.poc.todo.domain.entity.MoviePoster
import com.poc.todo.utils.getReadable
import com.squareup.picasso.Picasso
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_movie.*

class HomeViewHolder(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(movie: MoviePoster?, listener: (Long) -> Unit) {
        if (movie != null) {
            with(movie) {
                moviePoster.contentDescription = title
                Picasso.get().load(ResourceUrl.getPosterUrl(posterPath))
                    .placeholder(R.drawable.poster_placeholder).into(moviePoster)
                val rating = voteAverage / 10 * 5
                ratingBar.rating = rating
                movieReleaseDate.text = releaseDate.getReadable()
                itemView.setOnClickListener { listener.invoke(this.id) }
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup): HomeViewHolder {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_movie, parent, false)
            return HomeViewHolder(view)
        }
    }
}