package com.poc.todo.ui.home

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.poc.todo.domain.entity.MoviePoster
import com.example.cktravelapp.ui.fragments.home.HomeViewHolder

class HomeAdapter (private val listener: (Long) -> Unit) :
    PagedListAdapter<MoviePoster, RecyclerView.ViewHolder>(REPO_COMPARATOR) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return HomeViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val movieItem = getItem(position)
        if (movieItem != null) {
            (holder as HomeViewHolder).bind(movieItem, listener)
        }
    }

    companion object {
        private val REPO_COMPARATOR = object : DiffUtil.ItemCallback<MoviePoster>() {
            override fun areItemsTheSame(oldItem: MoviePoster, newItem: MoviePoster): Boolean =
                oldItem.id == newItem.id

            override fun areContentsTheSame(oldItem: MoviePoster, newItem: MoviePoster): Boolean =
                oldItem == newItem
        }
    }
}