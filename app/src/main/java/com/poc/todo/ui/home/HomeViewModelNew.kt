package com.example.cktravelapp.ui.fragments.home

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.lifecycle.ViewModel
import com.poc.todo.data.TaskData
import com.poc.todo.domain.tasklist.TaskListUseCase
import com.poc.todo.domain.vo.Direction
import com.poc.todo.domain.vo.LoadingStatus
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class HomeViewModelNew @Inject constructor(private val taskListUseCase: TaskListUseCase) : ViewModel() {
    val tasks = taskListUseCase.getTasks()

    //PagedList use BoundaryCallback object to send us callback about necessary events related to
    // data loading. Here we capture those events and fetch data from the network. The
    // movieListUseCase.fetchMore() function returns loading status which we observe in UI to
    // show progress bar.
    val loadingStatus: LiveData<LoadingStatus> = Transformations.switchMap(
        taskListUseCase.getBoundaryState()
    ) { onBoundaryItemLoaded(it.itemData, it.direction) }

    private fun onBoundaryItemLoaded(itemDate: Date, direction: Direction): LiveData<LoadingStatus> {
        Timber.d("onBoundaryItemLoaded %s %s ", itemDate, direction)
        return taskListUseCase.fetchMore(itemDate, direction)
    }

    fun insertTasks() {
        taskListUseCase.insertTasks(getTaskList())
    }

    private fun getTaskList(): List<TaskData> {
        return listOf(
            TaskData(1, "Test Title1", "test task data1", "", Calendar.getInstance().time, false),
            TaskData(2, "Test Title2", "test task data2", "", Calendar.getInstance().time, false),
            TaskData(3, "Test Title3", "test task data3", "", Calendar.getInstance().time, false),
            TaskData(4, "Test Title4", "test task data4", "", Calendar.getInstance().time, false),
            TaskData(5, "Test Title5", "test task data5", "", Calendar.getInstance().time, false),
            TaskData(6, "Test Title6", "test task data6", "", Calendar.getInstance().time, false),
            TaskData(7, "Test Title7", "test task data7", "", Calendar.getInstance().time, false),
            TaskData(8, "Test Title8", "test task data8", "", Calendar.getInstance().time, false)
        )

    }
}