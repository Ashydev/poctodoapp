package com.example.cktravelapp.ui.fragments.home

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.poc.todo.R
import com.poc.todo.data.TaskData
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.item_my_day_task.*

class HomeViewHolderNew(override val containerView: View) :
    RecyclerView.ViewHolder(containerView), LayoutContainer {

    fun bind(taskData: TaskData?) {
        if (taskData != null) {
            with(taskData) {
                task_title.text = title + " date :: " + date
                //itemView.setOnClickListener { listener.invoke(this.id) }
            }
        }
    }

    companion object {
        fun create(parent: ViewGroup): HomeViewHolderNew {
            val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_my_day_task, parent, false)
            return HomeViewHolderNew(view)
        }
    }
}