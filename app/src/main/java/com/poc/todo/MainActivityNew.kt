package com.poc.todo

import android.annotation.SuppressLint
import android.graphics.Color
import android.os.Bundle
import android.view.DragEvent
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.Toast
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import com.poc.todo.ui.home.HomeFragmentNew
import com.poc.todo.utils.MovableFloatingActionButton
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import kotlinx.android.synthetic.main.activity_main.*


class MainActivityNew : AppCompatActivity(), View.OnDragListener {
    lateinit var collapsingToolbarLayout: CollapsingToolbarLayout
    private var llTopLeft: LinearLayout? = null
    private var llTopRight: LinearLayout? = null
    private var llBottomLeft: LinearLayout? = null
    private var llBottomRight: LinearLayout? = null
    private var dl: DrawerLayout? = null
    private var t: ActionBarDrawerToggle? = null
    private var nv: NavigationView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_new)
        setSupportActionBar(toolbar)
        collapsingToolbarLayout =
            this.findViewById(R.id.collapsingToolbar)
        llTopLeft = findViewById(R.id.ll_top_left)
        llTopRight = findViewById(R.id.ll_top_right)
        llBottomLeft = findViewById(R.id.ll_bottom_left)
        llBottomRight = findViewById(R.id.ll_bottom_right)

        val appBarLayout = findViewById<AppBarLayout>(R.id.appbar)
        appBarLayout.addOnOffsetChangedListener(object : AppBarLayout.OnOffsetChangedListener {
            internal var isShow = true
            internal var scrollRange = -1

            override fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.totalScrollRange
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.title = getString(R.string.app_name)
                    supportActionBar!!.setHomeButtonEnabled(true)
                    supportActionBar!!.setDisplayHomeAsUpEnabled(true)
                    isShow = true
                } else if (isShow) {
                    supportActionBar!!.setHomeButtonEnabled(false)
                    supportActionBar!!.setDisplayHomeAsUpEnabled(false)
                    collapsingToolbarLayout.title =
                        " "//careful there should a space between double quote otherwise it wont work
                    isShow = false
                }
            }
        })


        //collapsingToolbarLayout.title = getString(R.string.app_name)

        val fab = findViewById<View>(R.id.collapsing_toolbar_floating_action_button_q) as MovableFloatingActionButton
        val lp = fab.layoutParams as RelativeLayout.LayoutParams
        fab.layoutParams = lp


        //recyclerView.layoutManager = LinearLayoutManager(this)
        //recyclerView.adapter = SimpleRecyclerAdapter((1..30).map { "Item $it" })
        //recyclerView.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.VERTICAL))
        //fab.setOnTouchListener(this)
        llTopLeft!!.setTag(1)
        llTopLeft!!.setOnDragListener(this)
        llTopRight!!.setTag(2)
        llTopRight!!.setOnDragListener(this)
        llBottomLeft!!.setTag(3)
        llBottomLeft!!.setOnDragListener(this)
        llBottomRight!!.setTag(4)
        llBottomRight!!.setOnDragListener(this)

        dl = findViewById<View>(R.id.drawer_layout) as DrawerLayout
        t = ActionBarDrawerToggle(this, dl, toolbar, R.string.Open, R.string.Close)


        /*t = object : ActionBarDrawerToggle(
            this,
            dl,
            R.string.Open,
            R.string.Close
        ) {
            override fun onDrawerClosed(drawerView: View) {
                super.onDrawerClosed(drawerView)
                invalidateOptionsMenu()
            }

            override fun onDrawerOpened(drawerView: View) {
                super.onDrawerOpened(drawerView)
                invalidateOptionsMenu()
            }
        }*/


        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        dl!!.addDrawerListener(t!!)
        t!!.syncState()

        nv = findViewById<View>(R.id.nav_view) as NavigationView
        nv!!.setNavigationItemSelectedListener(object : NavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                val id = item.getItemId()
                when (id) {
                    R.id.nav_first_fragment -> Toast.makeText(
                        this@MainActivityNew,
                        "My Account",
                        Toast.LENGTH_SHORT
                    ).show()
                    R.id.nav_second_fragment -> Toast.makeText(
                        this@MainActivityNew,
                        "Settings",
                        Toast.LENGTH_SHORT
                    ).show()
                    R.id.nav_third_fragment -> Toast.makeText(
                        this@MainActivityNew,
                        "My Cart",
                        Toast.LENGTH_SHORT
                    ).show()
                    else -> return true
                }


                return true

            }
        })

// loading dashboard fragment
        val ft = supportFragmentManager.beginTransaction()
        ft.replace(R.id.content_main_frame, HomeFragmentNew())
        ft.commit()
    }

    @SuppressLint("ResourceType")
    override fun onDrag(v: View?, event: DragEvent?): Boolean {
        if (event!!.action == DragEvent.ACTION_DRAG_ENTERED) {
            when (v?.tag as Int) {
                1 -> {
                    //(v as? LinearLayout)?.setBackgroundColor(Color.GRAY)
                    llTopLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.red_900)))
                    llTopRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.pink_500)))
                    llBottomLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.purple_500)))
                    llBottomRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.orange_500)))


                    // Invalidate the view to force a redraw in the new tint
                    llTopLeft?.invalidate()
                    llTopRight?.invalidate()
                    llBottomLeft?.invalidate()
                    llBottomRight?.invalidate()

                    true
                }
                2 -> {
                    llTopLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.red_500)))
                    llTopRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.pink_900)))
                    llBottomLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.purple_500)))
                    llBottomRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.orange_500)))


                    // Invalidate the view to force a redraw in the new tint
                    llTopLeft?.invalidate()
                    llTopRight?.invalidate()
                    llBottomLeft?.invalidate()
                    llBottomRight?.invalidate()
                    true
                }
                3 -> {
                    llTopRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.red_500)))
                    llTopLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.pink_500)))
                    llBottomLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.purple_900)))
                    llBottomRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.orange_500)))


                    // Invalidate the view to force a redraw in the new tint
                    llTopRight?.invalidate()
                    llTopLeft?.invalidate()
                    llBottomLeft?.invalidate()
                    llBottomRight?.invalidate()
                    true
                }
                4 -> {
                    llTopRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.red_500)))
                    llTopLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.pink_500)))
                    llBottomLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.purple_500)))
                    llBottomRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.orange_900)))

                    // Invalidate the view to force a redraw in the new tint
                    llTopRight?.invalidate()
                    llTopLeft?.invalidate()
                    llBottomLeft?.invalidate()
                    llBottomRight?.invalidate()
                    true
                }
                else -> { // Note the block
                    /*llTopLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.colorAccent)))
                    llTopRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.colorRed)))
                    llBottomLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.cardview_light_background)))
                    llBottomRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.colorGreen)))

                    // Invalidate the view to force a redraw in the new tint
                    llTopRight?.invalidate()
                    llTopLeft?.invalidate()
                    llBottomLeft?.invalidate()
                    llBottomRight?.invalidate()*/
                }
            }

        } else if (event!!.action == DragEvent.ACTION_DRAG_EXITED) {
            llTopLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.red_900)))
            llTopRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.pink_900)))
            llBottomLeft?.setBackgroundColor(Color.parseColor(resources.getString(R.color.purple_900)))
            llBottomRight?.setBackgroundColor(Color.parseColor(resources.getString(R.color.orange_900)))

            // Invalidate the view to force a redraw in the new tint
            llTopRight?.invalidate()
            llTopLeft?.invalidate()
            llBottomLeft?.invalidate()
            llBottomRight?.invalidate()
        } else if (event!!.action == DragEvent.ACTION_DROP) {
            //handle the dragged view being dropped over a target view
            val dropped = event.localState as FloatingActionButton
            val dropTarget = v as LinearLayout
            //stop displaying the view where it was before it was dragged
            dropped.visibility = View.INVISIBLE

            /*//if an item has already been dropped here, there will be different string
            val text = dropTarget.text.toString()
            //if there is already an item here, set it back visible in its original place
            if (text == text1.getText().toString())
                text1.setVisibility(View.VISIBLE)
            else if (text == text2.getText().toString())
                text2.setVisibility(View.VISIBLE)
            else if (text == text3.getText().toString()) text3.setVisibility(View.VISIBLE)

            //update the text and color in the target view to reflect the data being dropped
            dropTarget.text = dropped.text*/
            dropTarget.setBackgroundColor(Color.BLUE)
        }
        return true
    }
}