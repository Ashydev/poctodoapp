package com.poc.todo.utils

import android.content.Context
import android.content.res.Resources
import android.widget.Toast
import java.text.SimpleDateFormat
import java.util.*

fun Context.toast(message: CharSequence) =
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
fun dp2px(dp: Int): Float = dp * Resources.getSystem().displayMetrics.density
fun Date.getReadable(): String {
    val dateFormat = SimpleDateFormat("dd MMM yy", Locale.getDefault())
    return dateFormat.format(this)
}