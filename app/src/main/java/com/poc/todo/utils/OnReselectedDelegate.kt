package com.poc.todo.utils

interface OnReselectedDelegate {
    fun onReselected()
}